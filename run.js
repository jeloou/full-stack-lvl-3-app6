var db = require('mongoose'),
    util = require('util'),
    repl = require('repl'),
    path = require('path'),
    fs = require('fs');

var DATABASE_HOST = 'localhost',
    DATABASE_NAME = 'db';

var DATABASE_URL = util.format(
  'mongodb://%s/%s', DATABASE_HOST, DATABASE_NAME);

var connect = function() {
  var options = {
    server: {
      socketOptions: {
	keepAlive: 1
      }
    }
  };

  db.connect(DATABASE_URL, options);
};

connect();

/*
  Loading models
*/

var apps = path.resolve('./app/');
fs.readdirSync(apps).forEach(function(app) {
  var models = path.join(apps, app, 'models.js')

  if (fs.existsSync(models)) {
    require(models);
  }
});

repl.start('> ').context.db = db;
