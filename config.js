var express = require('express'),
    db = require('mongoose'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    bodyparser = require('body-parser'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session),
    path = require('path'),
    util = require('util'),
    fs = require('fs');

module.exports = exports = function(app, passport) {
  /*
    MongoDb configuration
  */
  var DATABASE_HOST,
      DATABASE_NAME,
      DATABASE_USER,
      DATABASE_PASSWORD;

  switch (app.settings.env) {
  case 'production':
    DATABASE_HOST = process.env.DATABASE_HOST;
    DATABASE_NAME = process.env.DATABASE_NAME;
    DATABASE_USER = process.env.DATABASE_USER;
    DATABASE_PASSWORD = process.env.DATABASE_PASSWORD;
    break;

  case 'development':
    DATABASE_HOST = 'localhost';
    DATABASE_NAME = 'db';
    break;

  default:
    break;
  }

  var DATABASE_URL = util.format(
    'mongodb://%s/%s', DATABASE_HOST, DATABASE_NAME);

  var connect = function() {
    var options = {
      server: {
	socketOptions: {
	  keepAlive: 1
	}
      }
    };

    if (DATABASE_USER && DATABASE_PASSWORD) {
      options.user = DATABASE_USER;
      options.password = DATABASE_PASSWORD;
    }

    db.connect(DATABASE_URL, options);
  };

  connect();

  db.connection.on('error', function(err) {
    console.log(err);
  });

  db.connection.on('disconnected', function() {
    connect();
  });

  /*
    Loading models
  */

  var apps = path.resolve('./app/');
  fs.readdirSync(apps).forEach(function(app) {
    var models = path.join(apps, app, 'models.js')

    if (fs.existsSync(models)) {
      require(models);
    }
  });

  /*
    Sessions configuration
  */
  var SESSION_ID_KEY,
      COOKIE_SECRET;

  switch(app.settings.env) {
  case 'production':
    SESSION_ID_KEY = process.env.SESSION_ID_KEY;
    COOKIE_SECRET = process.env.COOKIE_SECRET;
    break;

  case 'development':
    SESSION_ID_KEY = 'macaroon'
    COOKIE_SECRET = 'very secret string';
    break;

  default:
    break;
  }

  /* 
     .bodyParser should be above .methodOverride
  */
  app.use(bodyparser.urlencoded());
  app.use(session({
    secret: COOKIE_SECRET,
    name: SESSION_ID_KEY,
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({
      url: DATABASE_URL
    })
  }));

  /*
    Configuring passport
  */

  var User = db.model('User');
  passport.serializeUser(function(user, fn) {
    fn(null, user.id);
  });

  passport.deserializeUser(function(id, fn) {
    User.findOne({_id: id}, function(err, user) {
      fn(err, user);
    });
  });

  passport.use(new LocalStrategy(
    function(username, password, fn) {
      User.findOne({username: username}, function(err, user) {
	if (err) return fn(err);
	if (!user || !user.authenticate(password)) {
	  fn(null, false, {
	    message: 'email or password incorrect'
	  });
	  return;
	}

	fn(null, user);
      });
    }));

  app.use(passport.initialize());
  app.use(passport.session());

  /*
    Adding favicon and serving compressed static files
  */
  app.use(express.static(path.join(__dirname, 'public')));

  /*
    Configuring views dir and engine
  */
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');
};
