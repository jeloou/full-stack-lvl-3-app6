var db = require('mongoose'),
    Todo = db.model('Todo');

module.exports = (function(app) {
  app.route('/todos')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .post(function(req, res) {
      var todo;

      todo = JSON.parse(req.body.model);
    })
    .get(function(req, res) {
      
    });

  app.route('/todos/:id')
    .all(function(req, res, next) {
      if (!req.user) return res.status(401).end();
      next();
    })
    .get(function(req, res) {
      
    })
    .put(function(req, res) {
      var todo;

      todo = JSON.parse(req.body.model);
    })
    .delete(function(req, res) {
      var todo;
      
    });
});
