var db = require('mongoose');

var ObjectId = db.Schema.ObjectId;
var Schema = new(db.Schema)({
  title: {type: String, trim: true, required: true},
  user: {type: ObjectId, ref: 'User', required: true},
  done: {type: Boolean, default: false},
});

Schema.methods.toJSON = function() {
  return {
    id: this._id,
    title: this.title,
    done: this.done,
    user: this.user.toJSON()
  };
};

Schema.statics.add = function(args, fn) {
  var payload, todo;

  payload = args.payload;
  payload.user = args.user;

  todo = new(this)(payload);
  todo.save(function(err) {
    if (err) throw err;
    fn(null, todo);
  });
};

Schema.statics.get = function(args, fn) {
  var user, id;

  user = args.user;
  id = args.id;

  this
    .findOne({user: user,  _id: id})
    .populate('user')
    .exec(function(err, todo) {
      if (err) throw err;

      if (!todo) {
	fn({
	  message: 'Oops, we couldn\'t find that',
	  code: 404
	});
	return;
      }

      fn(null, todo);
    });
};

Schema.statics.fetch = function(args, fn) {
  var user;

  user = args.user;
  this
    .find({user: user})
    .populate('user')
    .exec(function(err, todos) {
      if (err) throw err;
      fn(null, todos);
    });
};

Schema.statics.change = function(args, fn) {
  var user, id, payload;

  user = args.user;
  payload = args.payload;
  id = args.id;

  this
    .findOne({user: user, _id: id})
    .populate('user')
    .exec(function(err, todo) {
      var todo;

      if (err) throw err;

      if (!todo) {
	fn({
	  message: 'Oops, we couldn\'t find that',
	  code: 404
	});
	return;
      }

      todo.title = payload.title;
      todo.done = payload.done;

      todo.save(function(err) {
	if (err) throw err;
	fn(null, todo);
      });
    });
};

Schema.statics.remove = function(args, fn) {
  var user, key;

  user = args.user;
  id = args.id;

  this
    .findOne({user: user, _id: id})
    .populate('user')
    .exec(function(err, todo) {
      if (err) throw err;

      if (!todo) {
	fn({
	  message: 'Oops, we couldn\'t find that',
	  code: 404
	});
	return;
      }

      todo.remove(function(err) {
	if (err) throw err;
	fn(null, todo);
      });
    });
};

var Todo = db.model('Todo', Schema);
